import unittest
from activity import Activity


class TestActivity(unittest.TestCase):

    def test_eq(self):
        an = "com.test.package/MainActivity"
        ac = Activity(an)
        assert ac != "com.test.package/MainActivity"
        assert ac == Activity(an)

    def test_activity(self):
        activity_names = [
            "com.test.package/MainActivity",
            "com.test.package/NextActivity",
            "com.test.package/LastActivity",
        ]
        activities = []
        head = None
        for ac_name in activity_names:
            ac = Activity(ac_name)

            if head is not None:
                head.next.append(ac)
            ac.parent = head
            activities.append(ac)

            head = ac

        ac = activities[0]
        assert ac.activity_name == "com.test.package/MainActivity"
        assert ac.path == "//MainActivity"
        assert ac.next[0] == activities[1]

        ac = activities[1]
        assert ac.activity_name == "com.test.package/NextActivity"
        assert ac.path == "//MainActivity/NextActivity"
        assert ac.next[0] == activities[2]

        ac = activities[2]
        assert ac.activity_name == "com.test.package/LastActivity"
        assert ac.path == "//MainActivity/NextActivity/LastActivity"
        assert ac.next == []
