import unittest
from actions.tearDown.xmind_report_action import XMindReportAction
from actions.activity.start_activity_action import StartActivityAction


class DriverAgent:

    def __init__(self, data: dict, activity: list):
        self.data = data
        self.acs = activity
        self.iter = iter(self.acs)

        self.path = ""

    def dump(self):
        return self.data

    def get_activity(self):
        return next(self.iter)


class TestReportXMind(unittest.TestCase):
    action = XMindReportAction()
    ac_action = StartActivityAction()
    config = {"start_app": "com.test.package"}

    def test_a(self):
        a = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
        ]
        import random
        l = [random.choice(a) for i in range(25)]
        print(l)
        driver = DriverAgent(None, ["com.test.package/" + x for x in l])

        ac = None
        for i in range(len(l)):
            ac = self.ac_action.entrance(ac, self.config, driver)

        head = ac.get_head()
        f = []
        d = "/"
        for x in l:
            if x not in d:
                d = d + "/" + x
                f.append(d)
            else:
                d = d.split(x)[0] + x
        print(set(f))
        self.action.entrance(head, None, driver)
