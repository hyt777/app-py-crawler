import unittest
from actions.activity.init_activity_element_action import InitActivityElementAction
from activity import Activity


class DriverAgent:

    def __init__(self, data: dict, activity: list):
        self.data = data
        self.acs = activity
        self.iter = iter(self.acs)

    def dump(self):
        return self.data

    def get_activity(self):
        return next(self.iter)


class InitActivityElement(unittest.TestCase):
    action = InitActivityElementAction()
    config = {"start_app": "com.test.package"}
    data = {
        "payload": {
            "name": "1",
            "type": "1",

        },
        "children": [
            {
                "payload": {
                    "name": "2",
                    "type": "2"
                },
                "children": [
                    {
                        "payload": {
                            "name": "3",
                            "type": "3"
                        }
                    },
                ]
            },
        ]
    }

    def test_a(self):
        driver = DriverAgent(self.data, ["com.test.package/MainActivity"])
        ac = self.action.entrance(Activity("com.test.package/MainActivity"), self.config, driver)
        assert "5fd4589ed155ff292ffb6b888e21ae9a" in ac.elements
        assert "3f6d77f719d05a63223bf477bdd44ab4" in ac.elements
        assert "d8bb8367cc639ac47934a2de5f238d28" in ac.elements
