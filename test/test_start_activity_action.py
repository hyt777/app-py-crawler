import unittest
from activity import Activity
from actions.activity.start_activity_action import StartActivityAction


class DriverAgent:

    def __init__(self, data: dict, activity: list):
        self.data = data
        self.acs = activity
        self.iter = iter(self.acs)

    def dump(self):
        return self.data

    def get_activity(self):
        return next(self.iter)


class TestStartActivity(unittest.TestCase):
    action = StartActivityAction()
    config = {"start_app": "com.test.package"}
    data = {
        "payload": {
            "name": "TestElement",
            "type": "android.appwidget.AppWidgetHostView",

        },
        "children": []
    }

    def test_a(self):
        driver = DriverAgent(self.data, ["com.test.package/MainActivity"])
        ac = self.action.entrance(None, self.config, driver)

        assert ac.activity_name == "com.test.package/MainActivity"
        assert ac.path == "//MainActivity"
        assert ac.next == []

    def test_b(self):
        driver = DriverAgent(self.data,
                             [
                                 "com.test.package/MainActivity",
                                 "com.test.package/NextActivity",
                                 "com.test.package/LastActivity",
                                 "com.test.package/MainActivity",
                                 "com.test.package/OtherActivity",
                             ])
        ac = None
        for i in range(5):
            ac = self.action.entrance(ac, self.config, driver)

        head = ac.get_head()
        assert head.activity_name == "com.test.package/MainActivity"
        _, _next = head.next[0]
        assert _next.activity_name == "com.test.package/NextActivity"
        _, _next = _next.next[0]
        assert _next.activity_name == "com.test.package/LastActivity"
        assert head.next[1][-1].activity_name == "com.test.package/OtherActivity"

    def test_c(self):
        a = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
        ]
        import random
        l = [random.choice(a) for i in range(25)]
        print(l)
        driver = DriverAgent(self.data, ["com.test.package/" + x for x in l])

        ac = None
        for i in range(len(l)):
            ac = self.action.entrance(ac, self.config, driver)

        head = ac.get_head()
        ll = []

        def test(l: list, node):
            a = node.path
            l.append(a)
            for el, ac in node.next:
                test(l, ac)

        f = []
        d = "/"
        for x in l:
            if x not in d:
                d = d + "/" + x
                f.append(d)
            else:
                d = d.split(x)[0] + x
        from util import md5

        print(set(f))
        test(ll, head)
        print(ll)
        assert set(f) == set(ll)
        assert set(Activity.activities.keys()) == set([md5(x) for x in set(f)])
