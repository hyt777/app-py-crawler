import unittest
from activity import Activity
from actions.tearDown.pickle_activity_action import PickleActivityAction
from actions.init.load_activity_action import LoadActivityAction


class Crawler:
    pass

    def load_activity(self, a):
        pass


class TestPick(unittest.TestCase):
    ac = Activity("com.test.package/MainActivity")

    def test_save(self):
        act = PickleActivityAction()
        act.entrance(self.ac, {}, None)

        act = PickleActivityAction()
        act.entrance(self.ac, {"save": {
            "flag": True,
            "name": "test_pick"
        }}, None)

        laa = LoadActivityAction()

        obj = Crawler()
        obj.config = {
            "load": {
                "flag": True,
                "name": "test_pick"
            }
        }
        laa.init(obj)
        assert obj.activity == self.ac
