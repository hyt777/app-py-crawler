from util import md5


class Activity:
    activities = {}
    driver = None

    def __init__(self, activity_name):
        self.activity_name = activity_name
        self.elements = {}
        self.next = []
        self.parent = None
        self._path = None

        self.element = None
        self.action = "click"
        self.args = None

    @property
    def path(self):
        activity_name = self.activity_name.split("/")[-1]
        if self.parent is not None:
            self._path = self.parent.path + "/" + activity_name
        else:
            self._path = "//" + activity_name
        return self._path

    def key(self):
        return md5(self.path)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.activity_name

    def __eq__(self, other):
        if not isinstance(other, Activity):
            return False
        return self.activity_name == other.activity_name

    def clear_element(self):
        self.element = None
        self.action = "click"
        self.args = None

    def click(self, value):
        self.element = value
        self.action = "click"

    def send_key(self, value, text):
        self.element = value
        self.action = "send_key"
        self.args = text

    def get_head(self):
        if self.parent is not None:
            return self.parent.get_head()
        else:
            return self
