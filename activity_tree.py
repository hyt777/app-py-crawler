class Activity:
    # activities = {}
    driver = None

    def __init__(self, activity_name):
        self.activity_name = activity_name
        self.elements = []
        self.parent = None
        self.next = []

        self.element = None
        self.action = "click"
        self.args = None

    def __str__(self):
        return self.activity_name

    def __eq__(self, other):
        return self.activity_name == other.activity_name

    def click(self, value):
        self.element = value
        self.action = "click"

    def send_key(self, value, text):
        self.element = value
        self.action = "send_key"
        self.args = text

    def next_activity(self, driver, config):
        activity = self.get_activity(driver, config)
        self.element = None
        self.action = None

        ac = self.has_in_parents(activity)
        if ac is None:
            ac = self.has_in_children(activity)
            if ac is None:
                activity.parent = self
                self.next.append(activity)
                ac = activity
        return ac

    def has_in_children(self, activity):
        if activity in self.next:
            return self.next[self.next.index(activity)]
        return None

    def has_in_parents(self, activity):
        if self.activity_name == activity.activity_name:
            return self
        elif activity in self.next:
            return self.next[self.next.index(activity)]
        else:
            if self.parent is not None:
                return self.parent.has_in_parents(activity)
            else:
                return None

    def get_head(self):
        if self.parent is not None:
            return self.parent.get_head()
        else:
            return self

    @classmethod
    def get_activity(cls, driver, config):
        activity_name = driver.get_activity()
        if activity_name is None:
            activity_name = driver.get_activity()
        return Activity(activity_name)
