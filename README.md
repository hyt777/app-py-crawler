### appPyCrawler
> python+appcrawler = appPyCrawler  

基于airtest+poco的app自动遍历工具  
该框架的核心思路来自于 思寒大佬做的[appcrawler](https://github.com/seveniruby/AppCrawler)   
但由于大佬是基于Scala编写的，对于我来说运行起来有点不方便  

并且蒙阴与目前AirTest下poco框架的发展，相比于appium来说稳定了许多  
因此有了编写一个airtest+poco的基于python的app爬虫测试框架

### 安装依赖
python 版本为 3.6

##### airtest安装
```shell script
pip install airtest
```


### 配置文件
config.yml
```yaml
devices: # airtest 链接设备的字符串
  - Android://127.0.0.1:5037/127.0.0.1:62025?cap_method=JAVACAP

start_app: cn.ehanghai.android.hex # 启动的应用的packa
start_action_list: # 启动应用后的动作列表，只在应用启动后按顺序执行一次
  - action: wait # 等待
    info: 10
  - action: click # 点击，
    info:
      name: cn.ehanghai.android.hex:id/statement_agree
  - action: swipe # 滑动，info可以选择 left，right，也可以直接写点（本质是 pos1,pos2 = eval("(0.8, 0.5), (0.1, 0.5)")）
    info: (0.8, 0.5), (0.1, 0.5)

select_list: # 筛选列表，首先对控件进行初步筛选
  - enabled: true
    visible: true
    type: android.widget.ImageView
  - enabled: true
    visible: true
    type: android.widget.TextView
  - enabled: true
    visible: true
    type: android.widget.ImageButton
  - enabled: true
    visible: true
    editalbe: true
    type: android.widget.EditText
  - enabled: true
    visible: true
    checkable: true
    type: android.widget.CheckBox


first_list: # 优先列表，出现以下控件优先点击
  - name: cn.ehanghai.android.hex:id/cancle_tv
  - name: cn.ehanghai.android.hex:id/iv_map_close

trigger_dict: # 遇到对应的控件，触发对应的操作
  修改昵称:
    target:
      name: cn.ehanghai.android.hex:id/change_name
    trigger:
      action: send_key
      text: _(:3」∠❀)_
      time: 1

back_list: # 返回按钮，一般最后点击
  - name: cn.ehanghai.android.hex:id/iv_back
  - name: 转到上一层级
  - text: 确定
  - text: 保存

black_list: # 黑名单
  - text: 相册
```

### 运行
```shell script
python main.py
```

### 运行逻辑

程序注意的运行逻辑如下
```python
class Crawler:
    def __init__(self):
        self.config = yaml.load(open("config.yml", encoding="utf-8"))
        self.driver = Driver(self.config)
        
        self.init_actions = [  # 初始化用的action
            StartAppInitAction(),  # 启动app设置
            StartActionInitAction(),  # app启动后的动作设置
        ]

        self.actions = [SelectAction(),  # 初步进行控件筛选
                        FilterAction(),  # 对特定的控件进行筛选
                        BlackAction(),  # 将在黑名单中的控件排除
                        FirstAction(),  # 优先点击的控件，执行run后续不执行
                        TriggerAction(),    # 触发器，遇到特定控件进行特定的操作，执行run后续不执行
                        BackHeadAction(),   # 将返回类型的按键单独取出来
                        NormalAction(),     # 一般的控件，每个控件默认点击一次，执行run后续不执行
                        BackEndAction(),    # 前面的都没有执行，那么点击返回按钮
                        ]
    def crawler(self):
        self.init()
        while self.run():
            pass

    def init(self):
        for action in self.init_actions:
            action.run(self.config, self.driver)

    def run(self):
        for action in self.actions:
            if not action.entrance(activity, self.config, self.driver):
                return True
        return False
```

主要是按顺序执行action,可以随意插入action来扩充功能
后续是希望可以通过配置文件来动态配置action










