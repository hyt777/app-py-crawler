import logging
import time
import os

DEFAULT_PATH = "Android_{}".format(time.time())


def init_logging():
    logger = logging.getLogger("crawler")
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        fmt='[%(asctime)s][%(levelname)s]<%(name)s> %(message)s',
        datefmt='%I:%M:%S'
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    fh = logging.FileHandler(os.path.join(DEFAULT_PATH, "log.txt"), mode='w', encoding="utf-8")
    fh.setFormatter(formatter)
    logger.addHandler(fh)


def get_logger(name):
    return logging.getLogger(name)


def init_logger():
    global logger
    os.makedirs(DEFAULT_PATH)
    init_logging()
    return get_logger("crawler")


logger = init_logger()
