from logger import logger


class Action:

    def __init__(self):
        self.tag = self.__class__.__name__

    def init(self, crawler):
        pass

    def entrance(self, activity, config, driver):
        """
        :return: 若返回True 则运行下一个action，若False 则直接进入新循环
        """
        logger.info("into {}".format(self.tag))
        if self.check(activity, config, driver):
            logger.info("start run func")
            self.run(activity, config, driver)
            return self.run_next_acton()
        return True

    def check(self, activity, config, driver):
        """
        检查，是否需要触发该 action
        :return:
        """
        return False

    def run(self, activity, config, driver):
        """
        :param activity: 当前界面标识
        :param config: 静态配置文件
        :param driver: 设备对象
        :return:
        """
        pass

    def run_next_acton(self):
        return False
