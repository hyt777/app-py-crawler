import os
import pickle

from actions.action import Action
from activity import Activity


class LoadActivityAction(Action):

    def init(self, crawler):
        config = crawler.config
        if config.get("load", {}).get("flag", False):
            load_name = config.get("load", {}).get("name", None)
            if load_name and os.path.exists(load_name):
                with open(load_name, "rb") as f:
                    ac = pickle.load(f)
                if isinstance(ac, Activity):
                    crawler.activity = ac
                    crawler.load_activity(ac)
