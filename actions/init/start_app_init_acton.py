# from actions.base.init_action import InitAction
from actions.action import Action


class StartAppInitAction(Action):

    def init(self, crawler):
        if "start_app" in crawler.config:
            crawler.driver.start_app(crawler.config["start_app"])

    # def check(self, activity, config, driver):
    #     return True
    #
    # def run(self, activity, config, driver):
    #     if "start_app" in config:
    #         driver.start_app(config["start_app"])
