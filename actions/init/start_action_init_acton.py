# from actions.base.init_action import InitAction
from actions.action import Action
from logger import logger
import time


class StartActionInitAction(Action):

    def init(self, crawler):
        if "start_action_list" not in crawler.config:
            return

        start_action_list = crawler.config["start_action_list"]
        for action in start_action_list:
            self._do_action(action, crawler.driver)

    #
    # def check(self, activity, config, driver):
    #     return True
    #
    # def run(self, activity, config, driver):
    #     if "start_action_list" not in config:
    #         return
    #
    #     start_action_list = config["start_action_list"]
    #     for action in start_action_list:
    #         self._do_action(action, driver)

    def _do_action(self, action, driver):
        action_tag = action["action"]

        logger.info(action)

        if action_tag == "wait" and "info" in action:
            time.sleep(int(action["info"]))

        if action_tag == "click" and "info" in action:
            driver.click(action["info"])

        if action_tag == "swipe" and "info" in action:
            info = action["info"]
            if info == "left":
                driver.swipe((0.9, 0.5), (0.1, 0.5))
            elif info == "right":
                driver.swipe((0.1, 0.5), (0.9, 0.5))
            else:
                try:
                    pos1, pos2 = eval(info)
                    driver.swipe(pos1, pos2)
                except Exception as e:
                    logger.error(e)
