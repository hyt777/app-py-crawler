from actions.action import Action
from activity import Activity
import xmind
import os


class XMindReportAction(Action):

    def entrance(self, activity: Activity, config, driver):
        package, name = activity.activity_name.split("/")

        # 1、如果指定的XMind文件存在，则加载，否则创建一个新的
        workbook = xmind.load("my.xmind")

        # 2、获取第一个画布（Sheet），默认新建一个XMind文件时，自动创建一个空白的画布
        sheet1 = workbook.getPrimarySheet()
        # 对第一个画布进行设计完善，具体参照下一个函数
        sheet1.setTitle(package)  # 设置画布名称

        # 获取画布的中心主题，默认创建画布时会新建一个空白中心主题
        root = sheet1.getRootTopic()
        root.setTitle(name)  # 设置主题名称

        def temp(node, activity):
            for _, ac in activity.next:
                _, name = ac.activity_name.split("/")
                sub = node.addSubTopic()
                sub.setTitle(name)
                temp(sub, ac)

        temp(root, activity)

        xmind.save(workbook, path=os.path.join(driver.path, "report.xmind"))
