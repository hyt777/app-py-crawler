from actions.action import Action
import pickle


class PickleActivityAction(Action):

    def entrance(self, activity, config, driver):
        if config.get("save", {}).get("flag", False):
            save_name = config.get("save", {}).get("name", None)
            if save_name and isinstance(save_name, str):
                with open(save_name, "wb") as f:
                    pickle.dump(activity, f)
