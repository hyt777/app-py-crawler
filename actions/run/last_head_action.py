from actions.action import Action
from util import select_element_for_dict, remove_the_same_element
from logger import logger


class LastHeadAction(Action):

    def check(self, activity, config, driver):
        if "last_list" not in config:
            return False
        return True

    def run(self, activity, config, driver):
        last_dict = select_element_for_dict(activity.elements, config["last_list"])
        remove_the_same_element(activity.elements, last_dict)
        activity.last_dict = last_dict
        logger.info("save last_dict :{}".format(last_dict))

    def run_next_acton(self):
        return True
