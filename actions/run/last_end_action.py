from actions.action import Action
from util import get_item, remove_clicked_item
from logger import logger


class LastEndAction(Action):

    def check(self, activity, config, driver):
        # 检查 activity 是否拥有 back_dict
        if not hasattr(activity, "last_dict") or len(activity.last_dict) == 0:
            return False

        if not hasattr(activity, "last_clicked"):
            activity.last_clicked = {}

        # normal_dict中,要剔除已经被点过的控件
        remove_clicked_item(activity.last_clicked, activity.last_dict)

        if len(activity.last_dict) > 0:
            logger.info("last_dict:{}".format(activity.last_dict))
            return True

    def run(self, activity, config, driver):
        logger.info("last_dict: {}".format(activity.last_dict))
        value = get_item(activity.last_dict, activity.last_clicked)
        if value is not None:
            activity.click(value)

    def run_next_acton(self):
        return False
