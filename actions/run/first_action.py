from actions.action import Action
from util import select_element_for_dict, remove_the_same_element, get_item
from logger import logger


class FirstAction(Action):

    def check(self, activity, config, driver):
        if "first_list" not in config:
            return False
        # 当前页面拥有的控件
        first_dict = select_element_for_dict(activity.elements, config["first_list"])
        remove_the_same_element(activity.elements, first_dict)

        if len(first_dict) > 0:
            # 优先控件，不用纠结控件是否点击过，只要出现就点击
            # 直接将当前的控件贴activity中
            activity.first_dict = first_dict
            logger.info("first_dict: {}".format(first_dict))
            return True
        return False

    def run(self, activity, config, driver):
        value = get_item(activity.first_dict)
        if value is not None:
            activity.click(value)

    def run_next_acton(self):
        return False
