from actions.action import Action
from activity import Activity


class CheckAction(Action):
    # 检查是否在指定的app中
    flag = 0

    def check(self, activity: Activity, config, driver):
        if "start_app" not in config:
            return False
        check_time = config.get("check_time", 5)

        if self.flag >= check_time:
            raise Exception("")

        package = config["start_app"]
        if package in activity.activity_name:
            self.flag = 0
            return False
        else:
            return True

    def run(self, activity: Activity, config, driver):
        self.flag += 1
        driver.back()

    def run_next_acton(self):
        return False
