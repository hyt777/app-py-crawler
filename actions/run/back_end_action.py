from actions.action import Action
from activity import Activity
from util import get_item
from logger import logger


class BackEndAction(Action):

    def check(self, activity: Activity, config, driver):
        # 检查 activity 是否拥有 back_dict
        return hasattr(activity, "back_dict") and len(activity.back_dict) > 0

    def run(self, activity: Activity, config, driver):
        logger.info("back_dict: {}".format(activity.back_dict))
        value = get_item(activity.back_dict)
        if value is not None:
            activity.click(value)

    def run_next_acton(self):
        return False
