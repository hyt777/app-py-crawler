from actions.action import Action
from util import remove_clicked_item, get_item
from logger import logger


class NormalAction(Action):

    def check(self, activity, config, driver):
        # 当前页面返回的控件
        normal_dict = activity.elements

        if not hasattr(activity, "normal_clicked"):
            activity.normal_clicked = {}

        # normal_dict中,要剔除已经被点过的控件
        remove_clicked_item(activity.normal_clicked, normal_dict)

        if len(normal_dict) > 0:
            activity.normal_dict = normal_dict
            logger.info("normal_dict:{}".format(normal_dict))
            return True

        return False

    def run(self, activity, config, driver):
        normal_dict = activity.normal_dict
        value = get_item(normal_dict, activity.normal_clicked)

        if value is not None:
            activity.element = value
            activity.action = "click"

    def run_next_acton(self):
        return False
