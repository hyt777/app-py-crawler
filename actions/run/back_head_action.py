from actions.action import Action
from activity import Activity
from util import select_element_for_dict, remove_the_same_element
from logger import logger


class BackHeadAction(Action):

    def check(self, activity: Activity, config, driver):
        # 将 后退 控件，都收集到 back_dict 中
        if "back_list" not in config:
            return False
        return True

    def run(self, activity: Activity, config, driver):
        back_dict = select_element_for_dict(activity.elements, config["back_list"])
        remove_the_same_element(activity.elements, back_dict)
        activity.back_dict = back_dict
        logger.info("save back_dict :{}".format(back_dict))

    def run_next_acton(self):
        return True
