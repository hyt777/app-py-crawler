from actions.action import Action
from activity import Activity
from logger import logger


class EventBackAction(Action):
    def check(self, activity: Activity, config, driver):
        logger.info("start_app:{} activity:{}".format(config.get("start_app", ""), activity.activity_name))
        if "start_app" in config and config["start_app"] in activity.activity_name:
            return True
        return False

    def run(self, activity: Activity, config, driver):
        driver.back()

    def run_next_acton(self):
        return False
