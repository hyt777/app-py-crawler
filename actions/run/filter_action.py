from actions.action import Action
from activity import Activity
from logger import logger


class FilterAction(Action):

    def check(self, activity: Activity, config, driver):
        _filter(activity.elements)
        return False


def _filter(elements):
    for key in list(elements):
        value = elements[key]
        if not _pos_less_one(value):
            elements.pop(key)
            continue

        # if not _text_less_15(value):
        #     elements.pop(key)
        #     break


def _pos_less_one(value):
    # pos 必须小于 1
    if "pos" in value:
        w, h = value["pos"]
        logger.info("Filter {} {}".format(1 >= w >= 0 and 1 >= h >= 0, value))
        return 1 >= w >= 0 and 1 >= h >= 0
    return False


def _text_less_15(value):
    # text 的长度小于15
    text = value.get("text", "")
    return len(value.get("text", "")) < 15
