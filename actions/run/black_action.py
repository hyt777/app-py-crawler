from actions.action import Action
from activity import Activity
from logger import logger
from util import select_element_for_dict, remove_the_same_element


class BlackAction(Action):
    # 黑名单

    def check(self, activity: Activity, config, driver):
        if "black_list" not in config:
            return False
        return True

    def run(self, activity: Activity, config, driver):
        black_dict = select_element_for_dict(activity.elements, config["black_list"])
        remove = remove_the_same_element(activity.elements, black_dict)
        logger.info("black list remove：{}".format(remove))

    def run_next_acton(self):
        return True
