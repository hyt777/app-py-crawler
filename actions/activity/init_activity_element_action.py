from actions.action import Action
import collections
from element import walk_element


class InitActivityElementAction(Action):

    def entrance(self, activity, config, driver):
        elements = collections.OrderedDict()
        walk_element(elements, driver.dump())
        activity.elements = elements
        return activity
